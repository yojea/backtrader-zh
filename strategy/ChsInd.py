import backtrader as bt


class Chs(bt.Indicator):

    _mindatas = 1 # 至少需要两个参数

    lines = ('chs',)

    def __init__(self):
        """
        这里self.data0差不多就是array, 如果要利用bt的特性, minperiod, 必须在__init__构造它的方法
        """
        close_delay = bt.LineDelay(self.data0, ago=-5)
        # open_delay = bt.LineDelay(self.data1, ago=-5)
        self.l.chs = close_delay
