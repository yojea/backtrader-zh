from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import backtrader as bt
from backtrader.utils.datehelper import *


class StrategyBase(bt.Strategy):
    params = (
        ('timeframe', '1h'),
        ('comm', 10),
        ('size', 95),
        ('init_cash', 20000),
        ('start_date', 20210101),
        ('ended_date', 20121117),
    )
    CLOSE, HLC3, HLOC4, VWAP = range(0, 4)

    def notify_order(self, order):
        dtstr = self.data.datetime.datetime().strftime("%Y-%m-%d")
        if order.status == order.Submitted:
            self.log('ORDER %s SUBMITTED, id: %s, symbol: %s, create: %s, Size: %.4f, Price: %.4f' % (
                order.ordtypename().upper(),
                order.ref,
                order.data._name,
                date2str(bt.utils.num2date(order.created.dt)),
                order.size,
                order.price if order.price else 0
            ), dt=order.created.dt)
            return
        if order.status == order.Accepted:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            # self.log('ORDER ACCEPTED', dt=order.created.dt)
            return

        if order.status in [order.Expired]:
            self.log('ORDER BUY EXPIRED', dt=order.created.dt)

        elif order.status in [order.Completed]:
            self.log(
                'ORDER %s COMPLETED, id: %s, symbol: %s, create: %s, Size: %.4f, Price: %.4f, Amount: %.4f, Comm %.4f' %
                (order.ordtypename().upper(),
                 order.ref,
                 order.data._name,
                 date2str(bt.utils.num2date(order.created.dt)),
                 order.executed.size,
                 order.executed.price,
                 order.executed.value,
                 order.executed.comm), dt=order.executed.dt)
        elif order.status in [order.Margin]:
            self.log(
                'Order Failed, not cash', dt=order.executed.dt
            )
