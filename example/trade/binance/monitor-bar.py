import backtrader.indicators as btind
from datetime import timedelta
import okexbase

"""
合约多头双均线金叉策略
1. 发生金叉的时候, 如果价格突破肯特钠上轨就买入
2. 平仓的时候发生死叉就平仓
"""

# No = 非趋势
# PreTrend = 趋势形成中
# Trended = 趋势中
(NoTrend, DownTrend, PreTrend, UpTrend) = range(4)


class BarMonitorStrategy(okexbase.BaseStrategy):
    alias = ('BarMonitorStrategy',)

    params = (
        # period for the fast Moving Average
        ('period', 60 * 4),
        ('multiply', 10),
        ('bars', 3),
        # period for the slow moving average
    )

    def __init__(self):
        super().__init__()
        self.vol = btind.SMA(self.data.volume(-1), period=self.p.period)
        self.high_close = btind.Highest(self.data.close(-1), period=self.p.period)
        self.lower_close = btind.Lowest(self.data.close(-1), period=self.p.period)
        self.ret = btind.PctChange(self.data.close, period=1)

        self.vol_dt = None
        self.high_close_dt = None
        self.lower_close_dt = None
        self.up_ret_dt = None
        self.down_ret_dt = None

    def is_expire(self, dt, hours):
        bar_dt = self.data.datetime.datetime() + timedelta(hours=8)
        if dt is None or (bar_dt - dt).seconds / 3600 > hours:
            return True
        else:
            return False

    def next(self):
        if self.live_data:
            for d in self.datas:
                name = d._name
                bar_dt0 = d.datetime.datetime() + timedelta(hours=8)
                open0, high0, low0, close0, vol0, amount0 = d.open[0], d.high[0], d.low[0], d.close[0], d.volume[0], d.amount[0]
                print("name: %s, dt: %s, O: %.4f, H: %.4f, L: %.4f, C: %.4f, V: %.4f, A: %.4f" %
                      (name, bar_dt0, open0, high0, low0, close0, vol0, amount0))

            # # 放量报警
            # if self.data.volume[0] > self.vol * self.p.multiply and self.is_expire(self.vol_dt, 1):
            #     action = "%s放量报警" % ("上涨" if self.data.close[0] > self.data.open[0] else "下跌")
            #     self.send_msg(action, "无", dtstr)
            #     self.vol_dt = bar_dt
            #
            # # 价格突破报警
            # if self.data.close[0] > self.high_close[0] and self.is_expire(self.high_close_dt, 2):
            #     content = "当前%.2f突破过去%d小时价格%.2f高点" % (self.data.close[0], int(self.p.period / 60), self.high_close[0])
            #     self.send_msg("价格突破高点报警", content, dtstr)
            #     self.high_close_dt = bar_dt
            #     self.lower_close_dt = None
            #
            # if self.data.close[0] < self.lower_close[0] and self.is_expire(self.lower_close_dt, 2):
            #     content = "当前%.2f突破过去%d小时价格%.2f低点" % (self.data.close[0], int(self.p.period / 60), self.high_close[0])
            #     self.send_msg("价格突破低点报警", content, dtstr)
            #     self.lower_close_dt = bar_dt
            #     self.high_close_dt = None
            #
            # _ret = np.array(self.ret.get(size=self.p.bars))
            # if np.sum(_ret[_ret > 0]) >= self.p.bars and self.is_expire(self.up_ret_dt, 1):
            #     action = "连续%s报警" % ("上涨" if _ret[0] > 0 else "下跌")
            #     self.send_msg(action, "无", dtstr)
            #     self.up_ret_dt = bar_dt
            #     self.down_ret_dt = None
            #
            # if np.sum(_ret[_ret < 0]) >= self.p.bars and self.is_expire(self.down_ret_dt_ret_dt, 1):
            #     action = "连续%s报警" % ("上涨" if _ret[0] > 0 else "下跌")
            #     self.send_msg(action, "无", dtstr)
            #     self.down_ret_dt = bar_dt
            #     self.up_ret_dt = None


