from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
from . import Indicator


class FillNa(Indicator):
    '''
    Defined by J. Welles Wilder, Jr. in 1978 in his book *"New Concepts in
    Technical Trading Systems"* for the ATR

    Records the "true high" which is the maximum of today's high and
    yesterday's close

    Formula:
      - truehigh = max(high, close_prev)

    See:
      - http://en.wikipedia.org/wiki/Average_true_range
    '''
    alias = ('FillNa',)
    lines = ('fillna',)

    def next(self):
        if self.data.get(0) == 0:
            self.lines.fillna[0] = self.lines.fillna[-1]
        else:
            self.lines.fillna[0] = self.data[0]

    def once(self, start, end):
        darray = self.data.array
        larray = self.line.array

        for i in range(start, end):
            if darray[i] == 0:
                larray[i] = larray[i - 1]
            else:
                larray[i] = darray[i]
