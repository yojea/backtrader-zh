#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015, 2016, 2017 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import collections
import json
from .ccxtorder import CCXTFutureOrder
from backtrader import BrokerBase, Order
from backtrader.position import CCXTFuturePosition
from backtrader.utils.py3 import queue, with_metaclass
from backtrader.stores.binancestore import BinanceStore
from backtrader.utils import logger


class MetaCCXTFutureBroker(BrokerBase.__class__):
    def __init__(cls, name, bases, dct):
        '''Class has already been created ... register'''
        # Initialize the class
        super(MetaCCXTFutureBroker, cls).__init__(name, bases, dct)
        BinanceStore.BrokerFutureCls = cls

'''
如果有新的交易所加进来, 必须这样命名
'''


class BinanceFutureBroker(with_metaclass(MetaCCXTFutureBroker, BrokerBase)):
    '''Broker implementation for CCXT cryptocurrency trading library.
    This class maps the orders/positions from CCXT to the
    internal API of ``backtrader``.

    Broker mapping added as I noticed that there differences between the expected
    order_types and retuned status's from canceling an order

    Added a new mappings parameter to the script with defaults.

    Added a get_balance function. Manually check the account balance and update brokers
    self.cash and self.value. This helps alleviate rate limit issues.

    Added a new get_wallet_balance method. This will allow manual checking of the any coins
        The method will allow setting parameters. Useful for dealing with multiple assets

    Modified getcash() and getvalue():
        Backtrader will call getcash and getvalue before and after next, slowing things down
        with rest calls. As such, th

    The broker mapping should contain a new dict for order_types and mappings like below:

    broker_mapping = {
        'order_types': {
            bt.Order.Market: 'market',
            bt.Order.Limit: 'limit',
            bt.Order.Stop: 'stop-loss', #stop-loss for kraken, stop for bitmex
            bt.Order.StopLimit: 'stop limit'
        },
        'mappings':{
            'closed_order':{
                'key': 'status',
                'value':'closed'
                },
            'canceled_order':{
                'key': 'result',
                'value':1}
                }
        }

    Added new private_end_point method to allow using any private non-unified end point

    '''

    order_types = {Order.Market: 'market',
                   Order.Limit: 'limit',
                   Order.Stop: 'stop',  # stop-loss for kraken, stop for bitmex
                   Order.StopLimit: 'stop limit'}

    mappings = {
        'closed_order': {
            'key': 'status',
            'value': 'closed'
        },
        'canceled_order': {
            'key': 'status',
            'value': 'canceled'}
    }

    def __init__(self, broker_mapping=None, debug=False, **kwargs):
        super(BinanceFutureBroker, self).__init__()
        if broker_mapping is not None:
            try:
                self.order_types = broker_mapping['order_types']
            except KeyError:  # Might not want to change the order types
                pass
            try:
                self.mappings = broker_mapping['mappings']
            except KeyError:  # might not want to change the mappings
                pass
            try:
                self.future_config = broker_mapping["future_config"]
            except KeyError:
                pass

        self.store = BinanceStore(**kwargs)
        self.positions = collections.defaultdict(CCXTFuturePosition)

        self.cash = 0
        self.value = 0

        self.debug = debug
        self.indent = 4  # For pretty printing dictionaries

        self.notifs = queue.Queue()  # holds orders which are notified

        self.open_orders = list()

        self.logger = logger.getLogger(__name__)

        self.startingcash = 0
        self.startingvalue = 0

        # 启动初始化
        self.flush_position()

        # 初始化配置
        self.init_broker()

    def init_broker(self):
        # 初始化用户持仓方向
        if self.store.is_dual_sideposition() != self.future_config["dualSidePosition"]:
            self.store.setting_positionside(self.future_config["dualSidePosition"])

        # 初始化保证金方式
        for currency in self.store.currency:
            if currency.upper() == "USDT":
                continue
            symbol = currency + "/USDT"
            if self.positions[currency].isolated != self.future_config["isolated"]:
                self.store.setting_margintype(symbol, ioslated=self.future_config["isolated"])

            if self.positions[currency].lever != self.future_config["leverage"]:
                self.store.setting_leverage(symbol, leverage=self.future_config["leverage"])

    def flush_position(self, params={}):
        """
        binance交易所currency是btc的base，而okex是usdt，所以币安交易所的value其实是这个币的size
        """
        params = {
            "type": "future"
        }
        balance = self.store.get_wallet(params=params)
        self.logger.info("----------- Position -------------------")

        for currency in self.store.currency:
            if currency.upper() == "USDT":
                # 现金资产
                free_size = balance[currency]["free"]
                used_size = balance[currency]["used"]
                if currency in self.positions:
                    self.positions[currency].fix(free_size, used_size, 0)
                else:
                    self.positions[currency] = CCXTFuturePosition(currency, free_size, used_size, "FUTURE", 1, 0)
                self.logger.info("%s: free=%.10f, used=%.10f, entry_price=%.4f, leverage=%s, posside=%s, isolated=%s", currency, free_size, used_size, 0, 1, "NO", True)

            else:
                # 合约仓位
                future_symbol = currency + "USDT"

                for pos in balance["info"]["positions"]:
                    if pos["symbol"] != future_symbol:
                        continue
                    leverage = int(pos["leverage"])
                    entry_price = float(pos["entryPrice"])
                    free_size = float(pos["positionAmt"])
                    position_side = pos["positionSide"]
                    isolated = pos["isolated"]
                    isolatedWallet = float(pos["isolatedWallet"])
                    used_size = 0
                    if currency not in self.positions:
                        self.positions[currency] = CCXTFuturePosition(currency, free_size, used_size, "FUTURE", leverage, entry_price, isolatedWallet, position_side, isolated)
                    else:
                        self.positions[currency].fix(free_size, used_size, entry_price, isolatedWallet, leverage, position_side, isolated)
                    self.logger.info("%s: free=%.10f, used=%.10f, entry_price=%.4f, margin=%.4f, leverage=%s, posside=%s, isolated=%s",
                                     currency, free_size, used_size, entry_price, isolatedWallet, leverage, position_side, isolated)

        self.cash = self.positions["USDT"].free_size

    def load_opens_order(self, owner):
        orders = self.get_orders_open()
        if orders:
            for order in orders:
                for data in owner.datas:
                    if data.p.dataname == order[self.mappings["order_result"]["symbol"]]:
                        ccxt_order = CCXTFutureOrder(owner, data, order, {}, self.mappings, self.order_types)
                        self.open_orders.append(ccxt_order)

    def getposition(self, data) -> CCXTFuturePosition:
        '''Returns the current position status (a ``Position`` instance) for
        the given ``data``'''
        if data == "USDT":
            return self.positions["USDT"]
        currency = data.p.dataname.replace("/USDT", "")
        return self.positions[currency]

    """
    等于保证金+未实现的盈亏
    """
    def getposvalue(self, data):
        if data == "USDT":
            return self.getposition(data).total_size

        position = self.getposition(data)
        profit, _ = position.unrealized_profit(data)
        return profit + position.margin

    def fetch_market(self, symbol):
        return self.store.fetch_market(symbol=symbol)

    def getcash(self):
        # Get cash seems to always be called before get value
        # Therefore it makes sense to add getbalance here.
        # return self.store.getcash(self.currency)
        return self.cash

    """
    保证金就是已经被冻结的现金
    """
    def getmarginvalue(self, data=None):
        if data is None:
            return self.positions["USDT"].used_size
        position = self.getposition(data=data)
        return position.total_size * position.entry_price / position.lever

    # def getprofit(self, data):
    #     position = self.getposition(data=data)
    #     return position.total_size * (data.close[0] - position.entry_price)

    """
    本金算上未实现盈亏
    """
    def getvalue(self, datas=None):
        # return self.store.getvalue(self.currency)
        self.value = self.positions["USDT"].total_size * 1
        if datas:
            for _data in datas:
                currency = _data.p.dataname.replace("/USDT", "")
                if currency in self.positions:
                    pos = self.positions[currency]
                    profit, _ = pos.unrealized_profit(_data)
                    self.value += profit
        return self.value

    def get_notification(self):
        try:
            return self.notifs.get(False)
        except queue.Empty:
            return None

    def notify(self, order):
        self.notifs.put(order.clone())

    def next(self):
        if self.debug:
            self.logger.debug('Broker next() called')

        update_flag = False
        for o_order in list(self.open_orders):
            oID = o_order.ccxt_order['id']

            # Print debug before fetching so we know which order is giving an
            # issue if it crashes
            if self.debug:
                self.logger.debug('Fetching Order ID: %s', oID)

            # Get the order
            ccxt_order = self.store.fetch_order(oID, o_order.data.p.dataname)
            # self.logger.info("fetch_order: %s, %s", oID, o_order.data.p.dataname)

            # Check if the order is closed
            if ccxt_order[self.mappings['closed_order']['key']] == self.mappings['closed_order']['value']:
                dt = int(ccxt_order["info"][self.mappings["order_result"]["trade_dt"]])
                amount = ccxt_order[self.mappings["order_result"]["size"]]
                trade_price = ccxt_order[self.mappings["order_result"]["trade_price"]]
                o_order.fix_time(ccxt_order)
                o_order.execute(dt, amount, trade_price, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                o_order.completed()
                self.logger.info('Completed Order ID: %s', oID)
                self.notify(o_order)
                self.open_orders.remove(o_order)
                update_flag = True

        if update_flag:
            self.flush_position()

    def _submit(self, owner, data, exectype, side, amount, price, plimit, params):
        order_type = self.order_types.get(exectype) if exectype else 'market'
        created = int(data.datetime.datetime(0).timestamp()*1000)
        # Extract CCXT specific params if passed to the orderzxzz
        params = params['params'] if 'params' in params else params

        # binance 不支持这个字段
        if "tag" in params:
            del params["tag"]
        # params['created'] = created  # Add timestamp of order creation for backtesting
        try:
            self.logger.info("Submit: symbol=%s, order_type=%s, side=%s, amount=%s, price=%s, param=%s", data.p.dataname, order_type, side, amount, price, params)
        except Exception as e1:
            pass
        ret_ord = self.store.create_order(symbol=data.p.dataname, order_type=order_type, side=side,
                                          amount=amount, price=price, params=params)

        _order = self.store.fetch_order(ret_ord['id'], data.p.dataname)
        # self.logger.info("fetch_order: %s, %s", ret_ord['id'], data.p.dataname)

        order = CCXTFutureOrder(owner, data, _order, params, self.mappings, self.order_types)

        # plimit 可以认为是触发价
        # 买单和卖单其实就是信号价(不含滑点的价格)
        # 止损单其实就是止损触发价, 只有止损单这个值不为空, 市价单和限价单都为空
        if not order.plimit and plimit:
            order.plimit = plimit

        self.open_orders.append(order)
        order.accept(self)

        # 无论是提交买单还是卖单, 必然会锁定资产, 对于资产的free和used有重大变化, 需要刷新账户
        self.flush_position()

        # 如果是market的话, 这里的order status是close状态的, 在next那里还会notify一次, 会做两次notify
        # 这里只通知提交但是不成交的提醒
        if order.ccxt_order[self.mappings['closed_order']['key']] != self.mappings['closed_order']['value']:
            self.notify(order)
        return order

    def buy(self, owner, data, size, price=None, plimit=None,
            exectype=None, valid=None, tradeid=0, oco=None,
            trailamount=None, trailpercent=None,
            **kwargs):
        del kwargs['parent']
        del kwargs['transmit']
        return self._submit(owner, data, exectype, 'buy', size, price, plimit, kwargs)

    def sell(self, owner, data, size, price=None, plimit=None,
             exectype=None, valid=None, tradeid=0, oco=None,
             trailamount=None, trailpercent=None,
             **kwargs):
        del kwargs['parent']
        del kwargs['transmit']
        return self._submit(owner, data, exectype, 'sell', size, price, plimit, kwargs)

    def cancel(self, order):

        oID = order.ccxt_order['id']

        if self.debug:
            print('Broker cancel() called')
            print('Fetching Order ID: {}'.format(oID))

        # check first if the order has already been filled otherwise an error
        # might be raised if we try to cancel an order that is not open.
        ccxt_order = self.store.fetch_order(oID, order.data.p.dataname)

        if self.debug:
            print(json.dumps(ccxt_order, indent=self.indent))

        if ccxt_order[self.mappings['closed_order']['key']].lower() == self.mappings['closed_order']['value'].lower():
            self.logger.info('Cancel Order ID and return: %s', oID)
            return order

        ccxt_order = self.store.cancel_order(oID, order.data.p.dataname)

        if self.debug:
            print(json.dumps(ccxt_order, indent=self.indent))
            print('Value Received: {}'.format(ccxt_order[self.mappings['canceled_order']['key']]))
            print('Value Expected: {}'.format(self.mappings['canceled_order']['value']))

        # 重新获取下订单
        ccxt_order = self.store.fetch_order(oID, order.data.p.dataname)

        if ccxt_order[self.mappings['canceled_order']['key']].lower() == self.mappings['canceled_order']['value'].lower():
            self.open_orders.remove(order)
            self.logger.info('Cancel Order ID success: %s', oID)
            order.cancel()
            self.flush_position()
            self.notify(order)
        return order

    def get_orders_open(self, safe=False):
        return self.store.fetch_open_orders()

    def private_end_point(self, type, endpoint, params):
        '''
        Open method to allow calls to be made to any private end point.
        See here: https://github.com/ccxt/ccxt/wiki/Manual#implicit-api-methods

        - type: String, 'Get', 'Post','Put' or 'Delete'.
        - endpoint = String containing the endpoint address eg. 'order/{id}/cancel'
        - Params: Dict: An implicit method takes a dictionary of parameters, sends
          the request to the exchange and returns an exchange-specific JSON
          result from the API as is, unparsed.

        To get a list of all available methods with an exchange instance,
        including implicit methods and unified methods you can simply do the
        following:

        print(dir(ccxt.hitbtc()))
        '''
        endpoint_str = endpoint.replace('/', '_')
        endpoint_str = endpoint_str.replace('{', '')
        endpoint_str = endpoint_str.replace('}', '')

        method_str = 'private_' + type.lower() + endpoint_str.lower()

        return self.store.private_end_point(type=type, endpoint=method_str, params=params)


