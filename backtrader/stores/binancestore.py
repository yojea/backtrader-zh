#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2017 Ed Bartosh <bartosh@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import time
from datetime import datetime
from functools import wraps

import backtrader as bt
import ccxt
from backtrader.metabase import MetaParams
from backtrader.utils.py3 import with_metaclass
from ccxt.base.errors import NetworkError, ExchangeError, OrderNotFound
from backtrader.utils import logger


class MetaSingleton(MetaParams):
    '''Metaclass to make a metaclassed class a singleton'''

    def __init__(cls, name, bases, dct):
        super(MetaSingleton, cls).__init__(name, bases, dct)
        cls._singleton = None

    def __call__(cls, *args, **kwargs):
        if cls._singleton is None:
            cls._singleton = (
                super(MetaSingleton, cls).__call__(*args, **kwargs))

        return cls._singleton

'''
如果有新的交易所加进来, 必须这样命名
'''
class BinanceStore(with_metaclass(MetaSingleton, object)):
    '''API provider for CCXT feed and broker classes.

    Added a new get_wallet_balance method. This will allow manual checking of the balance.
        The method will allow setting parameters. Useful for getting margin balances

    Added new private_end_point method to allow using any private non-unified end point

    '''

    # Supported granularities
    _GRANULARITIES = {
        (bt.TimeFrame.Minutes, 1): '1m',
        (bt.TimeFrame.Minutes, 3): '3m',
        (bt.TimeFrame.Minutes, 5): '5m',
        (bt.TimeFrame.Minutes, 15): '15m',
        (bt.TimeFrame.Minutes, 30): '30m',
        (bt.TimeFrame.Minutes, 60): '1h',
        (bt.TimeFrame.Minutes, 90): '90m',
        (bt.TimeFrame.Minutes, 120): '2h',
        (bt.TimeFrame.Minutes, 180): '3h',
        (bt.TimeFrame.Minutes, 240): '4h',
        (bt.TimeFrame.Minutes, 360): '6h',
        (bt.TimeFrame.Minutes, 480): '8h',
        (bt.TimeFrame.Minutes, 720): '12h',
        (bt.TimeFrame.Days, 1): '1d',
        (bt.TimeFrame.Days, 3): '3d',
        (bt.TimeFrame.Weeks, 1): '1w',
        (bt.TimeFrame.Weeks, 2): '2w',
        (bt.TimeFrame.Months, 1): '1M',
        (bt.TimeFrame.Months, 3): '3M',
        (bt.TimeFrame.Months, 6): '6M',
        (bt.TimeFrame.Years, 1): '1y',
    }

    log = logger.getLogger(__name__)

    BrokerCls = None  # broker class will auto register
    BrokerFutureCls = None  # broker class will auto register
    DataCls = None  # data class will auto register

    exchange_config = {
        "options": {
            'recvWindow': 60000,
            "createMarketBuyOrderRequiresPrice": True,
            "warnOnFetchOpenOrdersWithoutSymbol": False
        },
        "enableRateLimit": True
    }

    broker_mapping = {
        'order_types': {
            bt.Order.Market: 'market',
            bt.Order.Limit: 'limit',
            bt.Order.Stop: 'stop_loss',  # stop-loss for kraken, stop for bitmex
            bt.Order.StopLimit: 'stop_loss_limit'
        },
        'mappings': {
            'closed_order': {
                'key': 'status',
                'value': 'closed'
            },
            'canceled_order': {
                'key': 'status',
                'value': 'canceled'
            },
            "order_result": {
                'id': 'id',
                'symbol': 'symbol',
                'exectype': 'type',
                'side': "side",  #
                'price': "price",  # 委托价
                'stop_price': "stopPrice",  # 止损止盈触发价
                'size': "amount",
                'trade_dt': "timestamp",  # 成交时间
                'order_time': "time",  # 成交时间
                'trade_time': "updateTime",  # 成交时间
                'trade_price': "average",  # 成交价
            },
        }
    }

    # 合约支持的LIMIT, MARKET, STOP[止损限价单], TAKE_PROFIT[止盈限价单], STOP_MARKET, TAKE_PROFIT_MARKET, TRAILING_STOP_MARKET
    broker_future_mapping = {
        'order_types': {
            bt.Order.Market: 'market',                  # 市价单
            bt.Order.Limit: 'limit',                    # 限价单
            bt.Order.Stop: 'stop_market',               # 止损市价单
            bt.Order.StopLimit: 'stop',                 # 止损限价单
            bt.Order.Profit: "take_profit_market",      # 止盈市价单
            bt.Order.ProfitLimit: "take_profit",        # 止盈限价单
            bt.Order.StopTrail: "trailing_stop_market"  # 跟踪止损市价单
        },
        'mappings': {
            'closed_order': {
                'key': 'status',
                'value': 'closed'
            },
            'canceled_order': {
                'key': 'status',
                'value': 'canceled'
            },
            "order_result": {
                'id': 'id',
                'symbol': 'symbol',
                'exectype': 'type',
                'side': "side",  #
                'price': "price",            # 委托价
                'stop_price': "stopPrice",   # 止损止盈触发价
                'size': "amount",
                'trade_dt': "updateTime",     # 成交时间
                'order_time': "time",        # 成交时间{info}
                'trade_time': "updateTime",  # 成交时间
                'trade_price': "average",    # 成交价
            },
        }
    }

    @classmethod
    def getdata(cls, *args, **kwargs):
        '''Returns ``DataCls`` with args, kwargs'''
        return cls.DataCls(*args, **kwargs)

    @classmethod
    def getbroker(cls, *args, **kwargs):
        '''Returns broker with *args, **kwargs from registered ``BrokerCls``'''
        return cls.BrokerCls(*args, **kwargs)

    @classmethod
    def getfuturebroker(cls, *args, **kwargs):
        '''Returns broker with *args, **kwargs from registered ``BrokerCls``'''
        return cls.BrokerFutureCls(*args, **kwargs)

    def __init__(self, exchange, currency, config, retries, debug=False, sandbox=False):
        self.exchange = getattr(ccxt, exchange)(config)
        if sandbox:
            self.exchange.set_sandbox_mode(True)
        self.retries = retries
        self.debug = debug
        self.currency = currency + ["USDT"]

    def get_granularity(self, timeframe, compression):
        if not self.exchange.has['fetchOHLCV']:
            raise NotImplementedError("'%s' exchange doesn't support fetching OHLCV data" % \
                                      self.exchange.name)

        granularity = self._GRANULARITIES.get((timeframe, compression))
        if granularity is None:
            raise ValueError("backtrader CCXT module doesn't support fetching OHLCV "
                             "data for time frame %s, comression %s" % \
                             (bt.TimeFrame.getname(timeframe), compression))

        if self.exchange.timeframes and granularity not in self.exchange.timeframes:
            raise ValueError("'%s' exchange doesn't support fetching OHLCV data for "
                             "%s time frame" % (self.exchange.name, granularity))

        return granularity

    def retry(method):
        @wraps(method)
        def retry_method(self, *args, **kwargs):
            for i in range(self.retries):
                if self.debug:
                    print('{} - {} - Attempt {}'.format(datetime.now(), method.__name__, i))
                time.sleep(self.exchange.rateLimit / 1000)
                try:
                    return method(self, *args, **kwargs)
                except (NetworkError, ExchangeError):
                    BinanceStore.log.info('%s - %s - Attempt %s', datetime.now(), method.__name__, i)
                    if i == self.retries - 1:
                        raise

        return retry_method
    """
    1. 设置合约账户的持仓模式为单向持仓模式
    """
    def setting_leverage(self, symbol, leverage=1):
        # for future in self.currency:
        #     if "USDT" in future:
        #         continue
        #     future_name = future + "/USDT"
        self.exchange.set_leverage(symbol=symbol, leverage=leverage)

    # 是否双向持仓模式
    def is_dual_sideposition(self):
        _order = self.exchange.fapiPrivate_get_positionside_dual()
        return _order["dualSidePosition"]

    # 设置持仓方向模式
    def setting_positionside(self, dual=False):
        params = {
            "dualSidePosition": "true" if dual else "false"
        }
        _order = self.exchange.fapiPrivate_post_positionside_dual(params=params)
        return _order["code"] == "200"

    # 变化逐仓和全仓模式
    def setting_margintype(self, symbol, ioslated=True):
        marginType = "ISOLATED" if ioslated else "CROSSED"
        _order = self.exchange.set_margin_mode(symbol=symbol, marginType=marginType)
        return _order["code"] == "200"

    """
    获取现货钱包持仓信息
    """
    @retry
    def get_wallet(self, params=None):
        return self.exchange.fetch_balance(params)

    @retry
    def fetch_market(self, symbol):
        self.exchange.load_markets()
        markets = self.exchange.market(symbol)
        return markets

    """
    提交订单
    """
    @retry
    def create_order(self, symbol, order_type, side, amount, price, params):
        # returns the order
        return self.exchange.create_order(symbol=symbol, type=order_type, side=side,
                                          amount=amount, price=price, params=params)

    """
    取消订单
    """
    @retry
    def cancel_order(self, order_id, symbol):
        return self.exchange.cancel_order(order_id, symbol)

    """
    获取k线数据
    """
    @retry
    def fetch_ohlcv(self, symbol, timeframe, since, limit, params={}):
        if self.debug:
            print('Fetching: {}, TF: {}, Since: {}, Limit: {}'.format(symbol, timeframe, since, limit))
        return self.exchange.fetch_ohlcv(symbol, timeframe=timeframe, since=since, limit=limit, params=params)

    """
    获取指定id的订单详情
    """
    @retry
    def fetch_order(self, oid, symbol):
        return self.exchange.fetch_order(oid, symbol)

    """
    未成交的订单, 一般就是买卖单
    """
    @retry
    def fetch_open_orders(self):
        return self.exchange.fetchOpenOrders()

    @retry
    def private_end_point(self, type, endpoint, params):
        '''
        Open method to allow calls to be made to any private end point.
        See here: https://github.com/ccxt/ccxt/wiki/Manual#implicit-api-methods

        - type: String, 'Get', 'Post','Put' or 'Delete'.
        - endpoint = String containing the endpoint address eg. 'order/{id}/cancel'
        - Params: Dict: An implicit method takes a dictionary of parameters, sends
          the request to the exchange and returns an exchange-specific JSON
          result from the API as is, unparsed.

        To get a list of all available methods with an exchange instance,
        including implicit methods and unified methods you can simply do the
        following:

        print(dir(ccxt.hitbtc()))
        '''
        return getattr(self.exchange, endpoint)(params)
