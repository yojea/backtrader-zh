#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015-2020 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


import backtrader as bt
from backtrader.utils.datehelper import *
from backtrader import TimeFrame
import pandas as pd
import os

"""
打印持仓
"""


class Holdings(bt.TimeFrameAnalyzerBase):

    params = (
        ('timeframe', None),
        ('compression', None),
        ('csv', False),
        ('out', None),
        ('rounding', 6)
    )

    def start(self):
        super(Holdings, self).start()
        self.date_format = "%Y-%m-%d" if self.timeframe == TimeFrame.Days else "%Y-%m-%d %H:%M:%S"

    # 当前周期已经over的时候就计算一次回撤
    def on_dt_over(self):
        positions = self.strategy.broker.positions

        rs = []
        for data in self.datas and positions:
            position = positions[data]
            size, price = position.size, position.price
            # if size:
            rs.append([data._name, size, price])

        if rs:
            self.rets[self.dtkey] = rs

    def stop(self):
        self.on_dt_over()

        if self.p.csv and self.p.out:
            params = self.get_params()
            report_dir = "_".join(map(lambda x: str(x), params.values()))
            out = os.path.join(self.p.out, report_dir)
            if not os.path.exists(out):
                os.makedirs(out)

            fpath = os.path.join(out, "holdings.csv")
            self.to_dataframe().to_csv(fpath, index=False)

    def to_dataframe(self):
        cols = ["date", "symbol", "size", "price"]
        data = []
        _skip_headers = False
        for date, v in self.rets.items():
            if _skip_headers:
                _skip_headers = False
                continue

            for v1 in v:
                data.append([date2str(date, self.date_format)] + v1)

        df = pd.DataFrame(columns=cols, data=data)
        df = df.round(decimals=self.p.rounding)
        return df
