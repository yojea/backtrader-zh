#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015-2020 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


import collections
import os
import backtrader as bt
from backtrader import Order, Position
from backtrader.utils.datehelper import *
import pandas as pd
import itertools
"""
成交历史
"""


class Trades(bt.Analyzer):
    '''This analyzer reports the trades history occurred with each an every data in
    the system

    It looks at the order execution bits to create a ``Position`` starting from
    0 during each ``next`` cycle.

    The result is used during next to record the transactions

    Params:

      - headers (default: ``True``)

        Add an initial key to the dictionary holding the results with the names
        of the datas

        This analyzer was modeled to facilitate the integration with
        ``pyfolio`` and the header names are taken from the samples used for
        it::

          'date', 'amount', 'price', 'sid', 'symbol', 'value'
          - amount: 持仓大小
          - price: 持仓平均价格
          - value: 持仓权益

    Methods:

      - get_analysis

        Returns a dictionary with returns as values and the datetime points for
        each return as keys
    '''
    params = (
        ('headers', False),
        ('_pfheaders', ('idx', 'symbol', 'opentime', 'closetime', 'hsize', 'hprice', 'hvalue', 'csize', 'cprice', 'cvalue', 'pos',
                        'comm', 'pnl', 'pnlcomm', 'pnlret', 'fundvalue', 'hlen')),
        ('fund', None),
        ('csv', False),
        ('out', None),
        ('rounding', 6)
    )

    def start(self):
        super(Trades, self).start()
        if self.p.fund is None:
            self._fundmode = self.strategy.broker.fundmode
        else:
            self._fundmode = self.p.fund
        self.rets["history"] = list()
        if self.p.headers:
            self.rets["history"].append(list(self.p._pfheaders[0:]))
        self.idx = 0

    def notify_trade(self, trade: bt.Trade):
        # trade第一次开仓和最后平仓才会通知, 中间加仓和减仓不通知
        if trade.status == trade.Closed:
            for h in trade.history:
                s = h.status
                # print(f"status: {s.status}, dt: {bt.num2date(s.dt)}, size: {s.size}, price: {s.price}, value: {s.value}, pnl: {s.pnl}, pnlcomm: {s.pnlcomm}")
            if not self._fundmode:
                value = self.strategy.broker.getvalue()
            else:
                value = self.strategy.broker.fundvalue

            symbol = trade.data._name
            item = [
                self.idx, symbol, date2str(trade.open_datetime()), date2str(trade.close_datetime()),
                trade.size, trade.price, trade.value, trade.closesize, trade.closeprice, trade.closevalue, -trade.closevalue / value,
                trade.commission, trade.pnl, trade.pnlcomm, trade.pnlcomm / (value - trade.pnlcomm), value, trade.barlen
            ]
            self.rets["history"].append(item)
            self.idx += 1

    def stop(self):
        if self.p.csv and self.p.out:
            params = self.get_params()
            report_dir = "_".join(map(lambda x: str(x), params.values()))
            out = os.path.join(self.p.out, report_dir)

            if not os.path.exists(out):
                os.makedirs(out)

            fpath = os.path.join(out, "trades.csv")
            self.to_dataframe().to_csv(fpath, index=False)

    def to_dataframe(self):
        cols = self.p._pfheaders
        _skip_headers = self.p.headers
        if _skip_headers:
            data = self.rets["history"][1:]
        else:
            data = self.rets["history"]

        df = pd.DataFrame(columns=cols, data=data)
        df = df.round(decimals=self.p.rounding)
        if len(df) > 0:
            for col in cols:
                if col in ['symbol', 'opentime', 'closetime', 'hlen']:
                    continue
                pad = df[col].astype(str).str.len().max()
                df[col] = df[col].astype(str).str.ljust(pad, "0")
        return df


