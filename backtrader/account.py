import os
import json


class Account:
    def __init__(self, name, apiKey, secret, password, params):
        self.name = name,
        self.apiKey = apiKey
        self.secret = secret
        self.password = password
        self.params = params


class AccountMgt:
    def __init__(self):
        home_dir = os.path.expanduser('~')
        account_path = os.path.join(home_dir, ".jupyter", "config.json")
        with open(account_path, 'r') as f:
            data = json.load(f)
        self.data = data

    def get_account(self, name):
        _d = self.data["account"][name]
        return Account(name, _d["apiKey"], _d["secret"], _d["password"], _d["params"])
