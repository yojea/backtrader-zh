#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015-2020 Daniel Rodriguez
# Copyright (C) 2015-2020 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


from backtrader.feeds import GenericCSVData


class BinanceFinanceCSVData(GenericCSVData):
    '''
    Parses a `Metatrader4 <https://www.metaquotes.net/en/metatrader4>`_ History
    center CSV exported file.

    Specific parameters (or specific meaning):

      - ``dataname``: The filename to parse or a file-like object

      - Uses GenericCSVData and simply modifies the params
    '''
    # 把OHLC的数据结构放弃掉, 自己自定义
    linesoverride = True

    # 额外加的列数据
    lines = ('open', 'high', 'low', 'close', 'volume', 'amount', 'openinterest', 'datetime')

    Open, High, Low, Close, Volume, Amount, Openinterest, DateTime = range(8)

    LineOrder = [Open, High, Low, Close, Volume, Amount, Openinterest, DateTime]

    # 读取数据的配置, 对应csv的哪一列, 索引从0开始
    params = (
        ('nullvalue', float('NaN')),
        ('dtformat', '%Y-%m-%d %H:%M:%S'),
        ('tmformat', '%H:%M:%S'),
        ('datetime', 0),
        ('open', 1),
        ('high', 2),
        ('low', 3),
        ('close', 4),
        ('volume', 5),
        ('amount', 6),
        ('openinterest', 10) # 这个每啥用, 主要是解决reamplefilter必须的bug
    )
