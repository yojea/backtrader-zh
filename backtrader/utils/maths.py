def floatnan(o):
    if not o:
        return float('NaN')
    if isinstance(o, float) or isinstance(o, int):
        return o
    if isinstance(o, str):
        return float(o)

def floatnone(o):
    if not o:
        return None
    if isinstance(o, float) or isinstance(o, int):
        return o
    if isinstance(o, str):
        return float(o)

def floatzero(o):
    if not o:
        return 0
    if isinstance(o, float) or isinstance(o, int):
        return o
    if isinstance(o, str):
        return float(o)