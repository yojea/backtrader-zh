import requests
import json


class Alert:
    def __init__(self, token, enable=False):
        self.api_url = "https://oapi.dingtalk.com/robot/send?access_token=%s" % token
        self.prefix = "交易提醒"
        self.enable = enable
        self.token = token

    def send(self, text):
        if not self.enable:
            return None
        headers = {'Content-Type': 'application/json;charset=utf-8'}
        json_text = self._msg(text)
        rs = requests.post(self.api_url, json.dumps(json_text), headers=headers)
        return rs.content

    def _msg(self, text):
        json_text = {
            "msgtype": "text",
            "at": {
                "atMobiles": [
                    "11111"
                ],
                "isAtAll": False
            },
            "text": {
                "content": text
            }
        }
        return json_text

    def format(self):
        text = f"-----------【交易提醒】---------\n" \
        "【策略】：{name}\n" \
        "【账户】：{account}\n" \
        "【资产】：{asset}\n" \
        "【时间】：{date}\n" \
        "【事件】：{action}\n" \
        "【详情】：{detail}\n"
        return text



if __name__ == "__main__":
    token = "694c7178821579aaabc9bb753497ceb456305c137f6e4a071c186b07d253511a"
    alert = Alert(token, False)
    params = {
        "name": "mecury",
        "asset": "ADA",
        "action": "发生金叉",
        "detail": "无",
        "date": "2021-12-23"
    }
    print(alert.format().format(**params))