#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
###############################################################################
#
# Copyright (C) 2015-2020 Daniel Rodriguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import argparse

import backtrader as bt
import backtrader.feeds as btfeeds
import backtrader.indicators as btind


class BidAskCSV(btfeeds.GenericCSVData):
    linesoverride = True  # discard usual OHLC structure
    # datetime must be present and last
    lines = ('bid', 'ask', 'datetime')
    # datetime (always 1st) and then the desired order for
    params = (
        # (datetime, 0), # inherited from parent class
        ('bid', 1),  # default field pos 1
        ('ask', 2),  # default field pos 2
    )


class St(bt.Strategy):
    params = (('sma', False), ('period', 3), ('period2', 5), )

    # self.data就是指的self.data0
    def __init__(self):
        if self.p.sma:
            sma1 = btind.SMA(self.data, period=self.p.period)
            sma2 = btind.SMA(sma1, period=self.p.period2)
            self.sma = sma1 + sma2

    def preonce(self, start, end):
        '''
        runonce=True才会出现
        It will be called during the "minperiod" phase of a "once" iteration
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"preonce: {dtstr}")

    def oncestart(self, start, end):
        '''
        It will be called when the minperiod phase is over for the 1st
        post-minperiod value

        Only called once and defaults to automatically calling once
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"oncestart: {dtstr}")

    def once(self, start, end):
        '''
        runonce=True才会出现
        Called to calculate values at "once" when the minperiod is over
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"once: {dtstr}")

    def prenext(self):
        '''
        从第一条数据开始, 在miniperiod 阶段中调用, 当满足miniperiod时候就不调用了
        比如ma(3), 前两条调用prenext, 第3条数据的时候调用next
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"prenext: {dtstr}")

    def nextstart(self):
        '''
        当period过了miniperiod的第一个bar的时候调用, 而且这个方法最终也会调用next()
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"nextstart: {dtstr}")

    def next(self):
        '''
        超过miniperod的时候就调用ma(3), 第3条的时候调用
        '''
        dtstr = self.data.datetime.datetime().isoformat()
        print(f"next: {dtstr}")
        # txt = '%4d: %s - Bid %.4f - %.4f Ask' % (
        #     (len(self), dtstr, self.data.bid[0], self.data.ask[0]))
        #
        # if self.p.sma:
        #     txt += ' - SMA: %.4f' % self.sma[0]
        # print(txt)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Bid/Ask Line Hierarchy',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument('--data', '-d', action='store',
                        required=False, default='../../datas/bidask.csv',
                        help='data to add to the system')

    parser.add_argument('--dtformat', '-dt',
                        required=False, default='%m/%d/%Y %H:%M:%S',
                        help='Format of datetime in input')

    parser.add_argument('--sma', '-s', action='store_true',
                        default=True,
                        required=False,
                        help='Add an SMA to the mix')

    parser.add_argument('--period', '-p', action='store',
                        required=False, default=3, type=int,
                        help='Period for the sma')

    return parser.parse_args()


def runstrategy():
    args = parse_args()

    cerebro = bt.Cerebro()  # Create a cerebro

    data = BidAskCSV(dataname=args.data, dtformat=args.dtformat)
    cerebro.adddata(data)  # Add the 1st data to cerebro
    # Add the strategy to cerebro
    cerebro.addstrategy(St, sma=args.sma, period=args.period)
    cerebro.run(runonce=False)


if __name__ == '__main__':
    runstrategy()
